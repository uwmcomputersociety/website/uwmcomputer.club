Title: First Club Meeting (Fall 2020)
Date: 2020-9-25 21:00
Category: News
Authors: Karl Patapoff

# Friday, Oct 2nd @ 6PM

Welcome back! We are having our first meeting this ***Friday at 6PM*** on our discord **#general** voice channel. [Invite link here][discord]

This will be a show and tell from the officers, and a meet and greet.

This meeting is an opportunity to have a say in what we do for the rest of the semester, so please attend if you can! *(late arrival ok)*

Even if you cannot attend, we will meet every week so there will be plenty of opportunities to participate.

[discord]: https://discord.gg/vB78eSX
