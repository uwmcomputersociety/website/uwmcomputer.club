Title: Retro Computing
Date: 2019-11-10 20:00
Category: Meetings
Authors: Winston Weinert

# When

Thursday **November 14th** at 6:30 PM - 7:30 PM

# Where

UWM **EMS Room E237** - down the hall from the IEEE-CS club room

# Topic

**Let's explore technology through history!** Bring your old retro
computers and devices! Come and see a [SGI Indy][indy] in action, a
[DOS][dos] workstation, and more!

See you there!

[indy]: http://www.obsolyte.com/sgi_indy/
[dos]: https://www.freedos.org/
