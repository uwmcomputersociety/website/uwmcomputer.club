Title: Next Two Meetings
Date: 2021-10-25
Category: Meetings
Authors: James

# Next Meeting: Tuesday, October 26th at 7pm

Hello everyone! Our next two meetings will both be focusing on Linux and how to use it. The first of these meetings will be this **Tuesday, October 26th at 7pm in EMS W120**, where we will be installing Linux on a Raspberry Pi and introducing the basics of using a Linux-based system. Next week we will be focusing more on some of the advanced things you can do with Linux, as well as going over a couple of the different distros you can use.

Time: 10/26/21 7:00 PM

Location: EMS W120

[discord]: https://discord.gg/vB78eSX

