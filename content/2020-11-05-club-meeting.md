Title: Club Meeting - Nov 6
Date: 2020-11-05 21:00
Category: News
Authors: Mike Castillo

###Meeting Topics: Git setup, Project

Our next club meeting will be this **Friday, November 6th at 6pm**.
Meeting [Discord link][discord].

Our next club meeting will be this Friday, November 6 at 6pm. Last club meeting we didn't get to the git tutorial, so we will cover that this meeting. We will also talk about the Phaser HTML5 game framework and see if it is a good fit for the project.

Hope to see you there!

[discord]: https://discord.gg/vB78eSX
