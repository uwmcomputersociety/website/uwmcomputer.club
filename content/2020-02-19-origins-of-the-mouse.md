Title: Origins of the Mouse
Date: 2020-2-19 13:40
Category: Meetings
Authors: Winston Weinert

# Topic

Ever wondered what early computing looked like?  How about the first
usage of a mouse?  Enter The Mother of All Demos — a 1968
computer-performance-of-sorts. Join us as we watch highlights of this
televised demo that introduced copy & paste, the mouse, and
information retrieval.

- [A playlist of highlights on YouTube](https://www.youtube.com/playlist?list=PLCGFadV4FqU2yAqCzKaxnKKXgnJBUrKTE) (Spoilers: we will be watching this!)
- [The full video on YouTube](https://www.youtube.com/watch?v=yJDv-zdhzMY)
- [A website about the demo](https://www.dougengelbart.org/content/view/209/448/)

We look forward to discussing the history of computer interfaces
together. There will be snacks!

# When

Friday **February 21th** at 6:00 PM to 7:00 PM

# Where

UWM **EMS Room E145**.

See you there!
