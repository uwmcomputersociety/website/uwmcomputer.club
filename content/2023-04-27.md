Title: Club Meeting – April 27th
Date: 2023-04-25
Category: Meetings
Authors: Soham

###MIDI tutorial

For this week's meeting, we will demonstrate music creation software. We will bring out the club's MIDI equipment for you to work with. Show us your groovy tunes!

April 27 @ 6:00

EMS E295
