Title: Build a Website!
Date: 2020-2-11 21:45
Category: Meetings
Authors: Winston Weinert

# Topic

Let's build a member website together! We invite you to make a page
available at our soon-to-be-live
[members.uwmcomputer.club](https://members.uwmcomputer.club/) site. We
welcome any content that meets UWM's values, including games, bios,
and jokes!

We will also be discussing how
[uwmcomputer.club](https://uwmcomputer.club/) is generated from
Markdown and deployed with a simple `git push`.

To participate, bring your laptop [with `git`
installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git),
or team up with another participant.

# When

Friday **February 14th** at 6:00 PM to 7:00 PM

# Where

UWM **EMS Room E145**. Please note this new meeting location.

See you there!
