Title: Club Meeting – February 23rd
Date: 2023-02-21
Category: Meetings
Authors: Soham
###Retrocomputing Lab Tour
This Thursday, we will be visiting UWM's Retrocomputing Lab, a collection of historical computers run by Professor Thomas Haigh of the history department. The lab contains a nifty collection of fully functioning computers from the '80s and '90s with original software, so you can run DOOM, die on The Oregon Trail, or realize why the ET video game was so awful. More information can be found at Professor Haigh's website: https://www.tomandmaria.com/Tom2/Retrolab.

February 23 @ 6:00

EMS E295
