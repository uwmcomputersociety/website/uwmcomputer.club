Title: We are refreshing our website
Date: 2019-10-29 17:45
Category: News
Authors: Winston Weinert

We are currently in the process of redoing our website. Please bear
with us during the process. In the meantime, please check the
[Slack][slack] for announcements and news.

# Interested in working on this website?

Great! This website is hosted on our Organization's GitLab. See
[gitlab.com/uwmcomputersociety/website/uwmcomputer.club][repo] for
further instructions.

# How is this website generated?

We are using the [Pelican][pelican] static website generator, written
in Python, to generate this content. This means we simply write
markdown documents, and Pelican automatically converts them to HTML
documents.

[slack]: https://ieeecsatuwm.slack.com/
[repo]: https://gitlab.com/uwmcomputersociety/website/uwmcomputer.club
[pelican]: https://blog.getpelican.com/
