Title: Meeting: Let's Learn Webdev - CSS
Date: 2019-10-28 18:00
Category: Meetings
Authors: Winston Weinert

# When

Thursday **October 31st** at 6:30 PM - 7:30 PM

# Where

UWM **EMS Room E237** - down the hall from the IEEE-CS club room

# Topic

**Learn web dev: CSS**. Cascading Style Sheets are important to building
websites because it changes the look and appearance of the page. Let’s
learn CSS together!

See you there!
