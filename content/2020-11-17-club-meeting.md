Title: Club Meeting - Nov 20
Date: 2020-11-17 21:00
Category: News
Authors: Mike Castillo, Karl Patapoff

###Meeting Topics: s-cape

Our next club meeting will be today **Friday, November 20th at 6pm**.
Meeting [Discord link][discord].

Karl will discuss the basics of using spritesheets and tile handling in Phaser.

Hope to see you there!

[discord]: https://discord.gg/vB78eSX
