Title: Club Meeting - Dec 7th
Date: 2021-12-3 18:00
Category: Meetings
Authors: Lisa

###Last meeting for Fall 2021

Alright everyone, last meeting for Fall 2021!!!

Make sure to come looking good because Jacob Dymond is bringing the professional photography gear for your professional headshot. It's something that we all have to do, so we might as well have fun with it.


Also, former UWM IEEE-CS president Ilan Hirschman is coming to hang out. Maybe dinner ~5pm and then chill discussion about jobs and Professor Boyland's secrets.


Also also, winter hacker rank. James Yeazel is providing prizes including but not limited to being the smartest IEEE-CSer of The Year Award.

Time: Dec 7th @ 7pm

Place: EMS W120
