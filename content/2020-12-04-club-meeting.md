Title: Club Meeting - Dec 4
Date: 2020-12-04 12:00
Category: News
Authors: Karl Patapoff

###Meeting Topics: s-cape

Our next club meeting will be today **Friday, December 4th at 6pm**.
Meeting [Discord link][discord].

Mike will discuss creating maps in Tiled, and then importing them into s-cape.

See you there.

[discord]: https://discord.gg/vB78eSX
