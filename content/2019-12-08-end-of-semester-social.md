Title: End of Semester Social
Date: 2019-12-08 20:20
Category: Meetings
Authors: Winston Weinert

*Note: This is the final general meeting of Fall 2019. Stay tuned as we
announce the Spring 2020 meeting schedule in February.*

# When

Thursday **December 12th** at 6:30 PM to 7:30 PM

# Where

UWM **EMS Room E237** &mdash; down the hall from the IEEE-CS club room

# Topic

To mark the final IEEE-CS meeting of this semester, join us to play board games. There will be food. It will be fun!

See you there!
