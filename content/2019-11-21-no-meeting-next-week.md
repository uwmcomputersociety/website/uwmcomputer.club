Title: No meeting next week!
Date: 2019-11-21 21:22
Category: Meetings
Authors: Winston Weinert

<img src='{attach}/images/thanksgiving.png'
     alt='Thanks giving table'
     class='inline-image' />

Next week Thursday &mdash; November 28th &mdash; is part of the
Thanksgiving holiday so there will be no meeting. Meetings will
continue the following Thursday &mdash; December 5th.

Happy holidays!

*[Image credit][thanksgiving]*

[thanksgiving]: https://commons.wikimedia.org/wiki/File:Thanks_giving_day.jpeg
