Title: Club Meeting Today 6PM
Date: 2020-10-09 21:00
Category: News
Authors: Karl Patapoff

# Friday, Oct 9nd @ 6PM

Club meeting **tonight at 6PM** on our discord **#general** voice channel. [Invite link here][discord]

Activities:

- Intro to Markdown
- HackerRank Challenges

Hope to see you there.

[discord]: https://discord.gg/vB78eSX
