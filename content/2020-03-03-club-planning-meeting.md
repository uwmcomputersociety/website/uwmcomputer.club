Title: Spring 2020 Club Planning Meeting
Date: 2020-3-03 22:00
Category: Meetings
Authors: Winston Weinert

# Topic

Join us to plan the future of IEEE-CS @ UWM! After the planning
discussion, we invite you to participate in a short, single
challenge HackerRank coding challenge. There will be snacks!

## Planning Topics

- Discuss travel opportunities open to members for next semester
- Discuss acquiring equipment
- Discuss leadership opportunities for members next semester &mdash;
  two officers are graduating and we will be filling those positions!
- Discuss long-term projects involving Android App Dev, Hosting
  websites & services, and Raspberry Pis
- Discuss future meeting topics

# When

Friday **March 6th** at 6:00 PM to 7:00 PM

# Where

UWM **EMS Room E145**.

See you there!
