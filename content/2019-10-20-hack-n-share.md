Title: Meeting: Hack 'n Share
Date: 2019-10-20 18:00
Category: Meetings
Authors: Winston Weinert

# When

Thursday **October 24th** at 6:30 PM - 7:30 PM

# Where

UWM **EMS** Room **E237** - down the hall from the IEEE-CS club room

# Topic

**Hack 'n Share!** Let's discuss our current computing related projects!
Everyone will get a chance to discuss project ideas and get feedback
from their peers. Need help getting started? You've come to the right
place.

See you there!
