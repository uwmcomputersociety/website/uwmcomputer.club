Title: Club Meeting - Sep 21st
Date: 2021-09-20
Category: News
Authors: James

# Tuesday, September 21th at 7pm

Hey everyone, our weekly meeting will be tomorrow, **Tuesday, September 21st, at 7pm in EMS W120**. We will be going over how to build a computer, starting from picking out your parts, then going through putting it all together and hopefully we'll even have time to show how to install an OS. We're also planning to stream the meeting online, and will update you on how to tune in soon.

Time: 9/21/21 7:00 PM

Location: EMS W120

[discord]: https://discord.gg/vB78eSX
