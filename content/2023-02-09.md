Title: Club Meeting – February 9th
Date: 2023-02-07
Category: Meetings
Authors: Soham
###ChatGPT meeting
This Thursday's meeting will be about recent developments in deep learning technology and ChatGPT. We will have a demonstration of how ChatGPT's text prediction works, using Python.
February 9 @ 6:00
EMS E295
