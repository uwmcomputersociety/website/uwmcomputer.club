Title: Club Meeting - March 11th
Date: 2021-03-08
Category: Meetings
Authors: Karl Patapoff

###Meeting Topics: Hacker Rank

Our next club meeting will be **Thursday, 11th at 7pm**.
Meeting [Discord link][discord].

We will be completing challenges again on Hacker Rank in groups!

See you there.

[discord]: https://discord.gg/vB78eSX
