Title: Club Meeting - Sep 28th
Date: 2021-09-27
Category: News
Authors: Raf

# Tuesday, September 28th at 7pm

Yooooo! Everyone I'll be doing a Unity3D demo tomorrow night Tuesday September 28th in our normal room EMS W120. I'll be assuming you have Unity installed (Unity 2021.20.1.f is what I suggested in #gamedev (on discord), but you can do pretty much anythin). We'll go over the Unity interface and probably make some weird physics thing together.

Time: 9/28/21 7:00 PM

Location: EMS W120

[Link to Unity3D][unity3d]

[discord]: https://discord.gg/vB78eSX
[unity3d]: https://unity3d.com/get-unity/download
