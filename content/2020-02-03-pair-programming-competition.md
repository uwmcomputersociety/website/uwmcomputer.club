Title: Pair Programming Competition!
Date: 2020-2-03 23:20
Category: Meetings
Authors: Winston Weinert

# Topic

Join us for a friendly programming competition. All skill levels are
welcome. People of different abilities will be paired to work on
programming tasks together. It will be fun, and we promise you'll
learn something! The winning pair will receive boot-able Linux
thumb-drives!

Make sure to bring your computer!

# When

Friday **February 7th** at 6:00 PM to 7:00 PM

# Where

UWM **Union Room W181** &mdash; located between the the UW Credit
Union and the Ballroom West
([Floorplan](https://uwm.edu/union/maps/)).

See you there!
