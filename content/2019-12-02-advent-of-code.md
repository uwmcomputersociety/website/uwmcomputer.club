Title: Advent of Code
Date: 2019-12-02 19:52
Category: News
Authors: Winston Weinert

This is a yearly reminder that [Advent of
Code](https://adventofcode.com/) is
happening. For the first 25 days of December, a programming challenge
is given. What makes AoC unique is the challenges are well written,
are well tested, and touch on many different topics in programming.

# Leaderboard

IEEE-CS @ UWM has a private leaderboard. We invite you to join. Visit
[this link](https://adventofcode.com/2019/leaderboard/private), enter
 the code `724028-be56ca07` and click "Join".

# Slack Channel

We invite Advent of Code participants to discuss the problems in the
[#code-challenges](https://ieeecsatuwm.slack.com/channels/code-challenges)
Slack channel.

# Other "Advent of X" events

The interested computing-focused student may enjoy the following:

- [A Language a Day](https://andrewshitov.com/2019/11/25/a-language-a-day-advent-calendar-2019/) 
- [Bekk Christmas sites](https://bekk.christmas/) &mdash; sites about
  CSS, Javascript, Kotlin, Machine Learning, Open Source, etc, etc.
- [F# Advent Calendar](https://sergeytihon.com/2019/11/05/f-advent-calendar-in-english-2019/)
- [Azure Advent Calendar](https://azureadventcalendar.com/)

Want to add something to this list? [Fork this site on
GitLab](https://gitlab.com/uwmcomputersociety/website/uwmcomputer.club).
