Title: Server Hosting Tutorial with Winston!
Date: 2019-12-02 20:20
Category: Meetings
Authors: Winston Weinert

<img src='{attach}/images/web_server.png'
     alt='Web server'
     class='inline-image' />

# When

Thursday **December 5th** at 6:30 PM to 7:30 PM

# Where

UWM **EMS Room E237** &mdash; down the hall from the IEEE-CS club room

# Topic

Join us and learn how to set up your own server in the cloud. We will
learn how to set up a website and host a Minecraft server! **Please
sign up for [GitHub's free student
pack](https://education.github.com/benefits), which includes $50 for
new Digital Ocean users.** We will be using [Digital
Ocean](https://www.digitalocean.com/) for the demonstration. Google
Cloud, Amazon Web Services, or Azure should work for this workshop as
well.

See you there!
