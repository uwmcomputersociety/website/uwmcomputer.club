Title: Your title here!
Date: 2020-10-22 12:12
Category: News
Authors: Your name here
Status: Draft
<!-- Remove the preceding line before publishing!  Make sure to update the
Title, Date, Category, Authors! -->

Your text here.

<!-- Delete the rest of this document.  It is an example of markdown
syntax. -->

# Hello!

Your markdown content *here* :).  Markup is powered by
[Python-Markdown][python-markdown].  See the [markdown syntax
rules][markdown-syntax].

# this is a h1

## this is a h2

### this is a h3

#### this is a h4

##### this is a h5

###### this is a h6

**This is bold text.**

*This is italicized text.*

> this is a quote

[this is a link to google](https://google.com/)
[this is a "reference link" to DuckDuckGo][ddg]

```
This is a code snippet
```
[python-markdown]: https://python-markdown.github.io/
[markdown-syntax]: https://daringfireball.net/projects/markdown/syntax
[ddg]: https://duckduckgo.com/
