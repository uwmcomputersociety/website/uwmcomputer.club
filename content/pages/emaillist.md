Title: Email list
Slug: emaillist

<!-- For now, these forms just send an email to the officers. I don't know if there's an API to work directly with the listserv so you'll have to do it manually. –Soham -->
<div>
    <header>
        <p>Use this form to subscribe to or unsubscribe from our email list:</p>
    </header>
    <form method="post" action="emaillist.php">
        <div>
            <input type="radio" name="action" value="subscribe" id="subscribe" required>
            <label for="subscribe">Subscribe :)</label><br>
            <input type="radio" name="action" value="unsubscribe" id="unsubscribe" required>
            <label for="unsubscribe">Unsubscribe :(</label><br>
        </div>
        <br>
        <div>
            Email: <input type="email" name="email" placeholder="Enter email" required>
        </div>
        <br>
        <div>
          <button type="submit">Submit</button>
        </div>
    </form>
</div>
