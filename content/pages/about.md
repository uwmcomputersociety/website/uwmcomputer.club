Title: About IEEE-CS @ UWM
Short_Title: About
Date: 2019-10-29 17:20
Modified: 2012-02-11 21:49
Category: Meta

We are the *Institute of Electrical and Electronics Engineers -
Computer Society* (IEEE-CS) student chapter at the University of
Wisconsin-Milwaukee.

# When do we meet?

During Spring 2024, we meet every Friday 5:00PM-6:30PM in EMS E295.

# How to stay in touch?

We primarily communicate over our [Discord server](https://discord.gg/vB78eSX "Link to Discord invite").

- [`https://discord.gg/vB78eSX`](https://discord.gg/vB78eSX)

In addition to Discord, the club officers are reachable via email at:
[ieeecs-officers@uwm.edu][ieeecs-officers].

Announcements posted to this website and to Discord are also posted to
the [ieeecs-announcements@uwm.edu][ieeecs-announcements] mailing
list. Anybody with an `uwm.edu` email can subscribe.

# Where is our club room?

We have a club room located in **EMS E268**. Feel free to stop by and
say hi!

[discord]: https://discord.gg/vB78eSX
[ieeecs-officers]: mailto:ieeecs-officers@uwm.edu
[ieeecs-announcements]: http://listserv.uwm.edu/mailman/listinfo/ieeecs-announcements
