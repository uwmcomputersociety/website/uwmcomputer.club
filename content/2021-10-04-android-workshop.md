Title: Android Workshop - Oct 5th
Date: 2021-10-04
Category: News
Authors: Lisa

# Android Workshop - Tuesday, October 5th at 7pm

Hello everyone!! There will be an Android workshop on Tuesday, October 5, at 7pm (our usual meeting time).

You have the option of either using Android Studio (recommended, it is easier) or a large folder. Please download your choice before coming to the meeting (it takes a while, if you choose the folder, make sure you extract it).

**Choice #1** <br>
Android Studio &#8594; [https://developer.android.com/studio][android-studio] <br>
Project &#8594; [Google Drive Link][project] <br>

**Choice #2** <br>
Folder download: <br>
Windows &#8594; [Google Drive Link][windows] <br>
Linux &#8594; [Google Drive Link][linux]

**Choice #3** <br>
Just sit back and watch the presentation

<hr>
View the meeting presentation here &#8594; [Google Slides Link][presentation]

Time: 10/04/21 7:00 PM

Location: EMS W120

[discord]: https://discord.gg/vB78eSX
[android-studio]: https://developer.android.com/studio
[project]: https://drive.google.com/file/d/1hgZycSakLQFuL1QnPQ5IrVVb2XBmy1-u/view?usp=sharing
[windows]: https://drive.google.com/file/d/1rkwb1T0G6H8K_x4wkSkfvUAz7duet9N5/view?usp=sharing
[linux]: https://drive.google.com/file/d/1k898iJ64JVHTIyT9U2AIH0IqPiiWSdZ6/view?usp=sharing
[presentation]: https://drive.google.com/file/d/1oC2nRoK0X0chxTmiA7I9v6fyoGSFTsh5/view?usp=sharing
