Title: First General Meeting of Spring 2020
Date: 2020-01-27 21:40
Category: Meetings
Authors: Winston Weinert

We are pleased to announce our general meeting time for Spring 2020!
**Every Friday at 6:00 PM to 7:00 PM** IEEE-CS @ UWM will host a
general meeting. We are presently working towards arranging a
semester-long meeting location in EMS. In the meantime we will be
meeting in the Student Union room W181.

# When

Friday **January 31st** at 6:00 PM to 7:00 PM.

# Where

UWM **Union Room W181** &mdash; located between the the UW Credit
Union and the Ballroom West
([Floorplan](https://uwm.edu/union/maps/)).

# Topic

Club members meet 'n greet. Come voice interest in future activities
such as programming projects, LAN parties, coding competitions, and
more! There will be snacks!

See you there!

# How was this meeting time decided?

[Previously]({filename}/2020-01-16-survey-for-preferred-meeting-time.md)
we polled our members for preferred meeting times. The times were on
Thursday and Friday. Unfortunately a scheduling conflict arose that
prevents some of the IEEE-CS club officers from helping out with
meetings on Thursday, so we selected the most popular time slot on
Fridays. We apologize for any inconvenience this might have caused.

<img src='{attach}/images/spring-2020-survey.png'
     alt='IEEE-CS Spring 2020 General Meeting Poll Results'/>

We hope this best serves the student population. Any feedback can be
directed towards
[ieeecs-officers@uwm.edu](mailto:ieeecs-officers@uwm.edu).
