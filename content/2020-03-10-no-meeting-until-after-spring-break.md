Title: No meetings until after spring break
Date: 2020-3-10 22:00
Category: News
Authors: Winston Weinert, Thomas Hoffman

This is an important announcement about the upcoming meetings and
other club information. In light of the [Spring Break
extension][covid19] and this Friday being the day before the break, we
will _not_ be having general meetings on Fri 3/13, 3/20, and
3/27. **Please stay tuned for more information towards the end of Spring
Break.**

Also, we are interested in hearing your opinions on how to make our
club better. The survey should only take about 5 minutes: [click
here][survey] to take the survey.

In addition, please fill out [this survey][emailentry] with your (UWM
preferred) email, to ensure you get notifications for upcoming club
activities, along with updates as to how general club meetings will
continue with the development of the COVID-19 situation. *Your email
will be added to the ieeecs-announcements@uwm.edu mailing list.*


Stay healthy (and wash your hands)!


[emailentry]: https://forms.gle/YRQjnPXNaHWyAiM29

[survey]: https://forms.gle/vS98LibkZmZcekiS7

[covid19]: https://uwm.edu/coronavirus/
