Title: Learn Machine Learing, Computer Vision with Josh
Date: 2019-10-31 18:00
Category: Meetings
Authors: Winston Weinert

# When

Thursday **November 7th** at 6:30 PM - 7:30 PM

# Where

UWM **EMS Room E237** - down the hall from the IEEE-CS club room

# Topic

**Let's learn Machine Learning & Computer Vision with Josh**. Come get
the scoop of what are neural networks and what they can do for you!

See you there!
