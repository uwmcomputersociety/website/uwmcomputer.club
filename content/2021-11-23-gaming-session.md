Title: Club Meeting - Nov 23rd
Date: 2021-11-23 8:38
Category: Meetings
Authors: James

###Meeting Topics: Gaming Session

Hi everyone, join us **Today, Tuesday November 23 at 7 pm in EMS W120** for a chill gaming session. We’ll have plenty of different games, including many party games and several different consoles, so come hang out with us to kickstart thanksgiving break.

Time: Nov 23rd @ 7pm

Place: EMS W120
