Title: Club Meeting - Oct 30
Date: 2020-10-27 12:00
Category: News
Authors: Mike Castillo

###Meeting Topics: Git setup, Project

Our next club meeting will be this **Friday, October 30th at 6pm**.
Meeting [Discord link][discord].

We will have a tutorial and assistance on setting up git on your system, as well as learning the most common basic commands. With any time leftover, we will continue brainstorming ideas and organizing our maze project.

Hope to see you there!

[discord]: https://discord.gg/vB78eSX
