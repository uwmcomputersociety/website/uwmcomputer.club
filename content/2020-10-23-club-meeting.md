Title: Club Meeting - Oct 23
Date: 2020-10-23 12:00
Category: News
Authors: Mike Castillo

Our next club meeting is today at 6pm. We will learn how the club project will work through Gitlab, including how to get your environment set up and how to submit code changes. Then we will start coming up with ideas on how the game should be designed. Continue to post your ideas in #suggestions in [discord], especially if you can't make it to the club meeting to share.

[discord]: https://discord.gg/vB78eSX
