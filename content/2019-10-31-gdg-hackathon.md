Title: See you at GDG DevFest!
Date: 2019-10-31 18:00
Category: News
Authors: Winston Weinert

On Saturday November 2nd the UWM Google Developer Group is hosting a
hackathon open to university students. Come sling code with us! See
[GDG's Meetup][meetup] page for more information. There will be prizes
and food. IEEE-CS members are excited to compete in this event. See
you there!


[meetup]: https://www.meetup.com/GDG-UWM/events/264945191/
