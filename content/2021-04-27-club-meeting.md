Title: Club Meeting - April 29th
Date: 2021-04-27
Category: Meetings
Authors: Karl Patapoff

###Meeting Topics: HackerRank

Our next club meeting is this **Thursday, April 29th at 7pm**. HackerRank contest this time. Our last club meeting of the semester will be on May 6th.

Meeting [Discord link][discord].

Until Thursday.

[discord]: https://discord.gg/vB78eSX
