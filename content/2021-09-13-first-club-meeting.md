Title: First Club Meeting (Fall 2021)
Date: 2021-9-13 12:00
Category: News
Authors: James

# Tuesday, September 14th at 7pm

Hi everyone, come join us at our first official club meeting this **Tuesday, September 14th at 7pm in EMS W120**. This will be our kickoff meeting where we go over basic club info and our plans for the semester, as well as get to know everybody a bit better. There will be games as well as VR.

Time: 9/14/21 7:00 PM

Location: EMS W120

[discord]: https://discord.gg/vB78eSX
