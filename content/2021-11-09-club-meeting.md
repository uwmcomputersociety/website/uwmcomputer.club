Title: Club Meeting - Nov 9th
Date: 2021-11-09 18:00
Category: Meetings
Authors: James

###Meeting Topics: HackerRank

Hey everyone, we will be having our second Hackerrank meeting this **Tuesday, November 9th at 7pm in EMS W120**! There will be an entirely new set of challenges to complete this time, so don't worry if you've already completed the previous one. Come have fun and hang out while doing some coding problems!

Time: Nov 9th @ 7pm

Place: EMS W120
