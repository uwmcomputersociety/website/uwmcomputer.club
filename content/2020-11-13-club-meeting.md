Title: Club Meeting - Nov 13
Date: 2020-11-13 15:00
Category: News
Authors: Mike Castillo, Karl Patapoff

###Meeting Topics: Git setup, Project

Our next club meeting will be today **Friday, November 13th at 6pm**.
Meeting [Discord link][discord].

We will be walking through the API of Phaser and discussing how we may apply it to the s-cape game.

Hope to see you there!

[discord]: https://discord.gg/vB78eSX
