Title: Club Meeting Friday, Oct 16 @ 6PM
Date: 2020-10-13 21:00
Category: News
Authors: Karl Patapoff


Club meeting **Friday at 6PM** on our discord **#general** voice channel. [Invite link here][discord]

Activities:

- Semester Project Ideas

We are brainstorming ideas for a fun semester long project that everyone can participate in, regardless of individual skill-level. If you have suggestions or feedback but can't make it to the meeting, drop by the #suggestions channel on our Discord workspace.

The club will be presenting at the upcoming Meet & Greet on Friday, October 23rd. We've been assigned the slot 2-2:15pm, which will be a precreated presentation. Following that we will have a breakout session in Collaborative Ultra. If you know anyone who wants more information about the club, that would be a good time to interact with the officers and ask any questions they have. If you just want to stop by and hang out online for a little while, that would be welcome as well.

[discord]: https://discord.gg/vB78eSX
