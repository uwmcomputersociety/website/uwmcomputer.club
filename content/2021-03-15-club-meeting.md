Title: Club Meeting - March 18th
Date: 2021-03-15
Category: Meetings
Authors: Mike Castillo

###Meeting Topics: s-cape

Our next club meeting is this **Thursday, March 18 at 7pm**. We plan to work on some aspect of s-cape and present the new feature. We can also spend some social time afterward before our spring break. During spring break no club meeting is scheduled.

Meeting [Discord link][discord].

See you there.

[discord]: https://discord.gg/vB78eSX
