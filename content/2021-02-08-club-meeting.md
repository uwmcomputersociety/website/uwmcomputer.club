Title: Club Meeting - Feb 8
Date: 2021-02-08
Category: Meetings
Authors: Karl Patapoff

###Meeting Topics: Hacker Rank

Our next club meeting will be **Friday, February 12th at 6pm**.
Meeting [Discord link][discord].

We will be completing challenges on Hacker Rank in groups! This is a fun way to problem solve together. Also, please fill out the survey for best club meeting time: [Survey Link][survey].

Hope to see you there.

[discord]: https://discord.gg/vB78eSX
[survey]: https://docs.google.com/forms/d/e/1FAIpQLSeaTTq5GZ1qzLlM1gvCosnkZiEcHiSTyw7MgKxi_-3wScdeeA/viewform
