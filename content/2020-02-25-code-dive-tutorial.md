Title: Code Dive Tutorial
Date: 2020-2-25 22:00
Category: Meetings
Authors: Winston Weinert

# Topic

Code diving, or code reading, is a valuable skill to practice:

> Indeed, the ratio of time spent reading versus writing is well over
> 10 to 1. We are constantly reading old code as part of the effort to
> write new code. …[Therefore,] making it easy to read makes it
> easier to write.

(Robert C. Martin, *Clean Code: A Handbook of Agile Software Craftsmanship*)

We will be perusing well known open source codebases, practicing this
valuable skill. With a little patience, even the most curious
application (mis)behavior is understood.

To get started, consider [this outline of the code-diving
process](https://github.com/aredridel/how-to-read-code/blob/master/how-to-read-code.md). Some
codebases to peruse:

- [Angry IP Scanner (Java network
  application)](https://github.com/angryip/ipscan)
- [OpenRCT2 (C++ game)](https://github.com/OpenRCT2/OpenRCT2/)
- [NetBSD userland commands
  (C)](http://cvsweb.netbsd.org/bsdweb.cgi/src/?only_with_tag=MAIN#dirlist)
  — see `bin`, `usr.bin`, `sbin`, `usr.sbin`
- [CleanBlog (Python with Flask, Browser
  JavaScript)](https://github.com/defshine/cleanblog)
- [underscore.js (JavaScript
  library)](https://underscorejs.org/docs/underscore.html)

Good code is readable without knowing much about the Programming
Language or Library; we welcome members of all skills to participate!

# When

Friday **February 28th** at 6:00 PM to 7:00 PM

# Where

UWM **EMS Room E145**.

See you there!
