Title: Club Project - Fall 2020
Date: 2020-10-16 20:00
Category: News
Authors: Mike Castillo

We've decided on a club project for the semester - **a maze solver**. We can work out the features over the next several weeks. Possible ones might include a sophisticated AI, monsters, random generation, interactive objects, and mini-games.

The interface will be web-based, and everyone can vote on the tool-chain they would like to use. We narrowed it down to three feasible choices. Please vote on the following:

**Possible tool-chains**

- Python with Django
- HTML/CSS/JS with NodeJS
- Webassembly with multiple languages

Webassembly would be the hardest because it doesn't yet support garbage collection, and would probably be limited to C or C++ or Rust.

The code will be hosted on our [Gitlab group](https://gitlab.com/uwmcomputersociety). Task planning will be done with the Gitlab Issue Tracker. Feature development will be made in branches, and submitted via pull request with designated code reviewers. Project methodology will use Test-Driven Development.

Continue to **post your suggestions** and your thoughts in the #suggestions [discord] channel. This project can go in whatever direction we collectively lead it.

[discord]: https://discord.gg/vB78eSX
