Title: Club Meeting - April 22nd
Date: 2021-04-19
Category: Meetings
Authors: Karl Patapoff

###Meeting Topics: HACKIOWA Recap

Our next club meeting is this **Thursday, April 22nd at 7pm**. We are planning for the HACKIOWA teams to talk about their experiences at the hackathon.

Meeting [Discord link][discord].

See you there.

[discord]: https://discord.gg/vB78eSX
