Title: Welcome Back! Fall 2020
Date: 2020-9-18 20:30
Category: News
Authors: Karl Patapoff

# Meeting Times

Welcome back! This semester may be a bit strange, but we'll be meeting virtually. Let us know the best meeting time!

Head [to our Google form](https://docs.google.com/forms/d/e/1FAIpQLSeaTTq5GZ1qzLlM1gvCosnkZiEcHiSTyw7MgKxi_-3wScdeeA/viewform) and select one of the meeting times. We will communicate the next meeting date/time here and on Discord.

For our first meeting we are planning a show and tell from the officers, as well as meeting and introducing all of our members.
