Title: First Club Meeting (Spring 2021)
Date: 2021-2-05 14:00:00
Category: News
Authors: Karl Patapoff

# Friday, Feb 5th @ 6PM

Welcome back! We hope you had a nice winter break. It's now time to start back up for Spring! Join us *tonight at 6PM* on our discord **#general** voice channel. [Invite link here][discord]

We will introduce each other and talk about club activities. Be on the lookout throughout the week for a poll where we will see what club meeting times work for people going forward.

Can't make it at 6PM, join our discord and participate asynchronously!

See you there.

[discord]: https://discord.gg/vB78eSX
