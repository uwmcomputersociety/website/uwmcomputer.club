Title: Club Meeting - May 6th
Date: 2021-05-03
Category: Meetings
Authors: Karl Patapoff

###Meeting Topics: Last Meeting for Spring 2021

Our next club meeting is this **Thursday, May 6th at 7pm**. It'll be social time to celebrate the semester end. Also, leave us feedback and let's reflect back on the semester!

Meeting [Discord link][discord].

Until then.

[discord]: https://discord.gg/vB78eSX
