Title: Club Meeting - Dec 11
Date: 2020-12-11 16:00
Category: News
Authors: Mike Castillo, Karl Patapoff

###Meeting Topics: End of Semester

Our next club meeting will be today **Friday, December 11th at 6pm**.
Meeting [Discord link][discord].

This will be our last club meeting of the semester. We are going to discuss what worked and what didn't, and gather ideas for what we should do next semester. We are looking for ideas for Fall 2021 as well, including on-campus events provided it is safe to do so by that time.

This next Spring 2021 semester will continue to be held over Discord, in online format. Our first meeting will be on **Friday, February 5 at 6pm**, with an earlier informal meeting a week before on the 29th at 6pm. Afterwards, meetings will take place based on peoples' schedules.

Hope to see you there, and good luck with finals everyone!

[discord]: https://discord.gg/vB78eSX
