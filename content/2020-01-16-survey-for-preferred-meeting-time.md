Title: Spring 2020 General Meeting Survey
Date: 2020-01-16 21:00
Category: News
Authors: Winston Weinert

In preparation for the upcoming semester, please take a moment to
[fill out this survey][doodle] to decide the preferred general meeting
time. Whichever survey result gets the most votes will become our
general meeting time this semester.

The survey will conclude on **Sunday January 26th** and the time of
our first meeting for the last week of January will be subsequently
announced. Check back for more details.

# How were the proposed meeting times chosen?

The four IEEE-CS officers shared availability for club activities, we
found the following three time-windows are available to club use:

- Monday 2–3:15 PM
- Thursday 7–8:15 PM
- Friday 5–8:15 PM

Then we cross-referenced the [UWM Schedule of Classes][soc], omitting
times on Monday due to conflicting with CS150, CS151, CS251, CS315 and
CS395. This resulted in proposing times on Thursday and Friday evenings.

We look forward to a fun semester of activities and opportunity.

[doodle]: https://doodle.com/poll/uuia84vyak75wk7r
[soc]: https://catalog.uwm.edu/course-search/?srcdb=2202&subject=COMPSCI
