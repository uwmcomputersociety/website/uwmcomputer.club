Title: Club Meeting - April 15th
Date: 2021-04-14
Category: Meetings
Authors: Mike Castillo

###Meeting Topics: HACKIOWA Teams

Our next club meeting is this **Thursday, April 15 at 7pm**. We are planning for the HACKIOWA teams to give a little description of what they plan to work on.

Meeting [Discord link][discord].

See you there.

[discord]: https://discord.gg/vB78eSX
