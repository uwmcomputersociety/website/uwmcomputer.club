#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'IEEE-CS Members'
SITENAME = 'IEEE-CS @ UWM'
SITEURL = ''

#THEME = 'simple'
THEME = 'themes/notmyidea-ieeecs'

PATH = 'content'

OUTPUT_PATH = 'public/'

TIMEZONE = 'America/Chicago'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

GITLAB_URL = 'https://gitlab.com/uwmcomputersociety/website/uwmcomputer.club/'

# Blogroll
LINKS = (
    ('University of Wisconsin — Milwaukee', 'https://uwm.edu/'),
    ('IEEE-CS Parent Organization', 'https://www.computer.org/'),
    ('Slack Workspace', 'https://ieeecsatuwm.slack.com/'),
    ('Discord Workspace', 'https://discord.gg/vB78eSX'),
    ('GitLab Organization', 'https://gitlab.com/uwmcomputersociety'),
)

# Social widget
SOCIAL = (
    ('Presence', 'https://uwm.presence.io/organization/institute-of-electrical-and-electronic-engineers-computer-society-2'),
    ('Announcements Mailing&nbsp;List', 'http://listserv.uwm.edu/mailman/listinfo/ieeecs-announcements')
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

STATIC_PATHS = ['images', 'extra/favicon.ico', 'extra/logo.png']

EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {'path': 'favicon.ico'},
    'extra/logo.png': {'path': 'logo.png'},
}

HOME_IN_NAVBAR = True  # Show a Home link in the navbar.
