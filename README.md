# uwmcomputer.club

This is the web website to replace [the old
site][old-uwmcomputer.club]. The new site is generated using
[Pelican][pelican]. This means no more editing HTML files directly;
instead we edit Markdown documents. One still has control over
presentation and layout, however, with the use of Pelican's templating
facilities.

## Getting started

What you need:

1. [Python 3][python3]
2. [pip][pip] &mdash; If you have Python 3.4 or later, you likely already
   have pip installed. Verify with `pip --version`.

Sidenote: Are you using a [Virtual Environment][venv]? If not, think
hard before continuing.

Finally run:

```sh
pip install -r requirements.txt  # Install pelican and Markdown
pelican --listen --autoreload    # Serve the website on localhost:8000
```

You should be able to visit the site at [localhost:8000][localhost]

## How do I …?

### How do I add a new post?

Copy `content/template.md` to `content/<date>-your-title-here.md`.  Example:

```
cp content/template.md content/2020-09-18-my-cool-announcement.md
```

Then edit your new post file.  Make sure to change the title or it will clobber
existing posts.

### <a name='update-css'></a> How do I update the CSS?

Most edits to the theme should be added to
`themes/notmyidea-ieeecs/static/css/tweaks.css`.  CSS gets messy real fast, so
it's best to keep our "local" modifications away from the "stock" theme our
website uses.  The following steps will outline how to ensure `tweaks.css` will
be loaded when the browser refreshes the page.  When developing, you may need
to type Control-F5 to bypass cache when reloading the page.  [Read more about
cache control and cache busting CSS here][cache-busting-css].

[cache-busting-css]: https://css-tricks.com/strategies-for-cache-busting-css/

#### 1. Update the `base.html` template

Visit `themes/notmyidea-ieeecs/templates/base.html` and locate the `link
rel="stylesheet"` element.  Increment the "version" number in the query
parameter (e.g. `?v50` → `?v51`).  The pertinent line looks like this:

```html
<link rel="stylesheet" href="{{ SITEURL }}/{{ THEME_STATIC_DIR }}/css/{{ CSS_FILE }}?v50" />
```

#### 2. Update the `main.css` stylesheet

Visit `themes/notmyidea-ieeecs/static/css/main.css`.  For each stylesheet in
the *Imports* section, update the query parameter.  For example, if you edited
`tweaks.css`, increment the query parameter (e.g. `?v50` → `?v51`) or add one
(e.g. `?v1`).  The import lines look like this:

```css
@import url("tweaks.css?v50");
```

#### 3. Add your changes to the `tweaks.css` stylesheet

Now that you have updated the URLs used for the stylesheets in the HTML and
CSS, you can make your changes to the `tweaks.css` stylesheet.  If you made it
this far, you're a winner!

### How do I add a new link to the footer?

There are two different places to add URLs to the footer.  There is the
**links** section and **social** section.  The steps to add a link to either
section is similar.

#### 1. Edit `pelicanconf.py`

Visit `pelicanconf.py` and find the sections that look the the following:

```python
# Blogroll
LINKS = (
    ('University of Wisconsin — Milwaukee', 'https://uwm.edu/'),
    ('IEEE-CS Parent Organization', 'https://www.computer.org/'),
    ('Slack Workspace', 'https://ieeecsatuwm.slack.com/'),
    ('Discord Workspace', 'https://discord.gg/vB78eSX'),
    ('GitLab Organization', 'https://gitlab.com/uwmcomputersociety'),
)

# Social widget
SOCIAL = (
    ('Presence', 'https://uwm.presence.io/organization/institute-of-electrical-and-electronic-engineers-computer-society-2'),
    ('Announcements Mailing&nbsp;List', 'http://listserv.uwm.edu/mailman/listinfo/ieeecs-announcements')
)
```

To add a link to the **links** section, add an element to the `LINKS` tuple
with the first element is the link text, and the second element is the URL.

To add a link to the **social** section, add an element to the `SOCIAL` tuple
with the first element is the link text, and the second element is the URL.

#### 2. Install a 16x16 pixel png into the `icons/` directory

Your link looks really cool!  Now let's add an icon to the left of the link.
First grab an icon to use.  Usually the target website's favicon will suffice.
In any case, you will want to convert it to a 16x16 png file.  Use your
favorite image editor, or simply install imagemagick (which is probably already
installed on your *nix box) and run this command:

```
convert -scale 16x16 original-icon.png icon.png
```

This will take `original-icon.png`, scale it down to 16x16 pixels, and write it
out to `icon.png`.  Don't have a PNG?  No problem, this command will convert
between image formats too.  Once converted, you can verify the image is the
right size & file type via `file icon.png`.  It should read like this:

```
my-prompt$ file icon.png
icon.png: PNG image data, 16 x 16, 8-bit colormap, non-interlaced
```

Looks right?  Great!  Copy over that `icon.png` into
`themes/notmyidea-ieeecs/static/images/icons/yourlinkname.png`.  Remember the
name you gave this file.  You will need to use it in the next step.

#### 3. Add a CSS rule to place the image next to the link

You will be editing `themes/notmyidea-ieeecs/static/css/tweaks.css` so be sure
to [follow the steps for updating CSS](#update-css) or some web browsers may
not load your stylesheet edits.

In `themes/notmyidea-ieeecs/static/css/tweaks.css` add a CSS rule like:

```css
.blogroll a[href*='yourlinkname.net'] {
    background-image: url('../images/icons/yourlinkname.png');
}
```

This will *select* `a` tags with a `href` attribute with a value ending in
`yourlinkname.net`.  Selection is limited to only such tags that are located
within an element with the `blogroll` CSS class.  The *declaration* simply
changes the background on the selected `a` tag.

Smash that F5 key, and voilà!, you have a new link in the footer!

## Tool documentation

- [Python documentation][pydocs]
- [Pelican documentation][pelicandocs]
- [Jinja2 documentation][jinja2docs] (used for HTML templating)
- [HTML documentation][htmldocs]
- [CSS documentation][cssdocs]

[pelicandocs]: https://docs.getpelican.com/en/stable/
[jinja2docs]: https://jinja.palletsprojects.com/
[cssdocs]: https://developer.mozilla.org/en-US/docs/Web/CSS
[htmldocs]: https://developer.mozilla.org/en-US/docs/Web/HTML
[pydocs]: https://docs.python.org/3/

## Project layout

Below is the output of
`tree -F -I 'venv|__pycache__'  --dirsfirst  uwmcomputer.club`
with many entries removed for clarity.

```
uwmcomputer.club
├── content/                                      # Meetings, announcements posts live here
│   ├── extra/                                    # Static content unique to our organization
│   ├── images/                                   # Images referenced in the posts
│   ├── pages/                                    # Non-post pages live here
├── public/                                       # Where the generated website is placed
├── themes/
│   └── notmyidea-ieeecs/                         # Our custom theme, based off notmyidea
│       ├── static/
│       │   ├── css/
│       │   │   ├── gitlab.css                    # For the gitlab banner
│       │   │   ├── main.css                      # The main css stuff lives here
│       │   │   ├── pygment.css
│       │   │   ├── reset.css
│       │   │   └── tweaks.css                    # Stuff to modify/override the theme defaults
│       │   ├── fonts/                            # Web fonts
│       │   └── images/
│       │       └── icons/                        # Bottom link icons live here, see also main.css
│       └── templates/                            # HTML templates
│           ├── base.html                         # The main template, includes other templates
│           └── gitlab.html                       # For the Fork me on gitlab banner
├── LICENSE.md                                    # License of the project
├── Makefile                                      # Try "make serve"
├── pelicanconf.py                                # Configuration of pelican & template parameters
├── publishconf.py                                # Same as above but only when publishing the site
├── README.md                                     # This README
├── requirements.txt                              # List of PyPI packages one needs to use pelican
└── tasks.py                                      # "Hooks" one can modify
```

## Contributing

If you want to change something, we invite you to make a Merge Request
against this repository. If you have questions, please feel free to
open an issue or ask on the Slack. We have a [#website
channel][website] on our Slack Workspace.

## License

This project is Unlicense. See [LICENSE.md][license].


[license]: LICENSE.md
[old-uwmcomputer.club]: https://gitlab.com/uwmcomputersociety/website/old-uwmcomputer.club
[pelican]: https://blog.getpelican.com/
[website]: https://ieeecsatuwm.slack.com/messages/C0AT7PD6D
[python3]: https://www.python.org/downloads/
[pip]: https://pip.pypa.io/en/stable/installing/
[localhost]: http://localhost:8000/
[venv]: https://virtualenv.pypa.io/en/stable/
